#Campaign summary#

##Description##

The main purpose of this tool is to collect all important Campaign's information in the single place to decrease the "human factor" and capture Campaign setting before delivery.

##How to use it##

* Save [THIS](https://bitbucket.org/andy_zy/campaign-summary/raw/f7c43f7f15496e19e05f61dcf6ed835e9c60c1b7/script.min.js) code as bookmark
* Go to the Campaign Settings page in the Admin UI

![ui.png](https://bitbucket.org/repo/8xyBXE/images/1013502704-ui.png)

* Bookmark's execution will replace current page content with a generated table

![table.png](https://bitbucket.org/repo/8xyBXE/images/4257150327-table.png)

* Press `Ctrl + A` and `Ctrl + C` to copy the summary table
* Paste it into the MS Word document

##Support##

Please, create a ticket using [THIS](https://bitbucket.org/andy_zy/campaign-summary/issues/new) link if you found a bug or have any suggestions.