(function($){
	var itemsQueue = [],
		doRequestAndStoreData = function(context, url, data){
			var	xsrfToken = (document.cookie.match(/XSRF-TOKEN=(.[^;]+)/) || [,''])[1];

			context.queue.push($.ajax({
				type: data ? 'POST' : 'GET',
				url: url,
				headers: {
					'X-Requested-With': 'XMLHttpRequest',
					'X-XSRF-TOKEN': xsrfToken
				},
				data: data || undefined
			}));
		},
		caption = (function(){
			var domain = $('.current-site-name').text(),
				cmpName = $('.mm-campaign-name span').text(),
				props = [].map.call($('.mm-campaign-name + .mm-subscribe .clearfix'), function(item){
					return $.trim($(item).text());
				}).join(', ');

			return 'Domain: ' + domain + '<br>Campaign: ' + cmpName + '<br><small style="font-size: 14px">' + props + ', Date: ' + new Date().toLocaleString() + '</small>';
		})(),
		summaryTable = $('<table class="summary mm-dashboard"><tr style="border: none"><th colspan="2" style="font-size:18px;font-weight:bold;">' + caption +'</th></tr></table>'),
		items = [
			{
				name: 'mappings',
				defer: {
					queue: [],
					getData: function(){
						var _this = this;

						$('.mm-CampaignLocations .mm-content .mm-links-panel a:contains("details")').each(function(){
							var href = $(this).attr('href');

							doRequestAndStoreData(_this, href);
						});
					}
				},
				parser: function(data){
					var $content = $('<tr class="headline" style="background-color: #555; color: #fff;"><th colspan="2">Mappings</th></tr>');

					if(data && data.length){
						data.forEach(function(res, i){
							var index = i + 1,
								$data = $(res[0]),
								$settings = $('<tr class="subline" style="background-color: #dadada; color: #525252;"><th colspan="2"></th></tr><tr class="settings"><td class="mapped" width="50%"></td><td class="props" width="50%"></td></tr>'),
								pageName = $data.find('.page-properties .page-name').html(),
								previewUrl = $data.find('.page-properties td:contains("Preview URL:") + td').html(),
								pageOrder = $data.find('.page-properties td:contains("Order:") + td').html();

							$settings.filter('.subline').find('th')
								.html(index + ') Page name: ' + pageName + ', Order: ' + pageOrder + ', Preview URL: ' + previewUrl);

							$settings.find('.mapped').append((function(){
								var wrap = $('<table width="100%"><tr><th width="50%">Item</th><th width="50%">Order</th></tr></table>');

								$data.find('.page-det-obj input:checked').each(function(){
									var maxyboxName = $data.find('[for=' + $(this).attr('id') + ']').html(),
										maxyboxOrder = $data.find('[id=' + $(this).attr('id').replace(/__.+$/, '__OutputOrder') + ']').val();

									wrap.append('<tr><td class="name">' + maxyboxName + '</td><td class="order" align="center">' + maxyboxOrder + '</td></tr>');
								});

								return wrap;
							})());

							$settings.find('.props').append((function(){
								var wrap = $('<table width="100%"><tr><th width="50%">Included Masks:</th><th width="50%">Excluded Masks:</th></tr><tr><td class="included" align="center"></td><td class="excluded" align="center"></td></tr>');

								$data.find('.page-prop-mask-table').find('.included, .excluded').each(function(){
									wrap.find('.' + $(this).attr('class')).append('<div>' + $(this).text() + '</div>');
								});

								wrap.find('.included:empty, .excluded:empty').html('N/A');

								return wrap;
							})());

							$content = $content.add($settings);
						});

						summaryTable.append($content);
					}
				}
			}, {
				name: 'elements',
				defer: {
					queue: [],
					getData: function(){
						var href = $('.mm-CampaignContent .mm-dashboard-title').attr('href');

						doRequestAndStoreData(this, href)
					}
				},
				parser: function(data){
					if(data && data[0]){
						var $data = $(data[0]),
							$content = $('<tr class="headline" style="background-color: #555; color: #fff;"><th colspan="2">Elements & Variants</th></tr>'),
							sanitize = function(node){
								node.attr('width', '100%');
								node.find('.edit-wrap input').each(function(){
									var originalValue = $(this).val(),
										newValue = originalValue == 100 ? originalValue : '<b class="excluded" style="font-weight: bold">' + originalValue + '</b>';

									$(this).replaceWith(newValue);
								});
								node.find('.edit-cell-wrap input[type="radio"]').each(function(){
									$(this).replaceWith($(this).is(':checked') ? '<b class="included">&#10004;</b>' : '<b class="excluded">&#10008;</b>');
								});
								node.find('.t-grid-header, .t-header, .t-alt').removeClass('t-grid-header t-header t-alt');
								node.find('.overflow-tooltip + div').remove();
								node.find('[class*="t-last"], tfoot').remove();
								node.find('th, td').filter(function(){
									return !$.trim($(this).html());
								}).remove();
								node.find('.edit-cell-wrap').attr('align', 'center');

								return node;
							};

						$data.find('.content-element-block, .campaign-content-master').each(function(i){
							var index = i + 1,
								$settings = $('<tr class="subline" style="background-color: #dadada; color: #525252;"><th colspan="2"></th></tr><tr class="settings"><td colspan="2"></td></tr>'),
								subline = $(this).hasClass('campaign-content-master') ? 'Sub-Campaigns configuration' : index + ') ' + $(this).find('.title-block').html() + ', Type of Variants: ' + $(this).find('.subscribe dt:contains("Type of Variants:") + dd').text();

							$settings.find('th').html(subline);
							$settings.find('td').append(sanitize($(this).find('.stylized-table')));

							$content = $content.add($settings);
						});

						summaryTable.append($content);
					}
				}
			}, {
				name: 'actions',
				defer: {
					queue: [],
					getData: function(){
						var href = $('.mm-CampaignActions .mm-dashboard-title').attr('href');

						doRequestAndStoreData(this, href)
					}
				},
				parser: function(data){
					if(data && data[0]){
						var $data = $(data[0]),
							$content = $('<tr class="headline" style="background-color: #555; color: #fff;"><th colspan="2">Actions</th></tr>'),
							$settings = $('<tr class="settings"><td colspan="2"></td></tr>'),
							sanitize = function(node){
								node.attr('width', '100%');
								node.find('th').attr('width', '25%');
								node.find('.edit-cell-wrap input[type="radio"]').each(function(){
									$(this).replaceWith($(this).is(':checked') ? '<b class="included">&#10004;</b>' : '<b class="excluded">&#10008;</b>');
								});
								node.find('.t-grid-header, .t-header, .t-alt').removeClass('t-grid-header t-header t-alt');
								node.find('[class*="t-last"], tfoot, colgroup').remove();
								node.find('.edit-cell-wrap, .edit-cell-wrap + td').attr('align', 'center');

								return node;
							};

						$settings.find('td').append(sanitize($data.find('#Grid > table')));
						$content = $content.add($settings);
						summaryTable.append($content);
					}
				}
			}, {
				name: 'targeting',
				defer: {
					queue: [],
					getData: function(){
						var _this = this,
							baseURL = location.href.replace(/campaign-[^/]+/, 'campaign-none').replace(/CampaignSettings[\s\S]*$/, '');

						doRequestAndStoreData(_this, baseURL + 'CampaignSegmentSummary/GetSummarySegmentRuleList', {});

						$('.mm-ContentTargeting .free-left-side-wrap a').each(function(){
							var segmentId = ($(this).attr('href').match(/\d+$/) || [''])[0];

							doRequestAndStoreData(_this, baseURL + 'CampaignSegmentSummary/GetSummarySegmentRule', {segmentId: segmentId});
						});
					}
				},
				parser: function(rules){
					if(rules && rules.length){
						var rulesOrder = rules.shift(),
							index = 1,
							$content = $('<tr class="headline" style="background-color: #555; color: #fff;"><th colspan="2">Segment Rules</th></tr>');

						if(rulesOrder && rulesOrder[0]){
							rulesOrder[0].forEach(function(ruleSummary){
								if( ruleSummary.IsEnabled ){
									rules.forEach(function(ruleData){
										var ruleConf = ruleData[0],
											operator = {
												'1': 'IS',
												'2': 'CONTAINS',
												'3': 'IS NOT'
											};

										if( ruleConf.Id === ruleSummary.Id ){
											var $settings = $('<tr class="subline" style="background-color: #dadada; color: #525252;"><th colspan="2"></th></tr><tr class="settings"><td colspan="2"><table width="100%"></table></td></tr>');

											$settings.find('th').html((index++) + ') ' + ruleConf.Name + ', Traffic: ' + ruleConf.TrafficPercent + ', Calculate Once: ' + ruleConf.CalculateOnce);

											ruleConf.Conditions.forEach(function(conf, i){
												var statement = i === 0 ? 'IF' : 'AND IF',
													ruleName = conf.AttributeName.split('/')[1] || conf.AttributeName,
													condition = operator[conf.Operator],
													value = conf.Values.map(function(item){
														return item.Value;
													}).join(',');

												$settings.find('td table').append('<tr><td width="25%"><i>' + statement + '</i></td><td width="25%">' + ruleName + '</td><td width="25%"><i>' + condition + '</i></td><td width="25%">' + value + '</td></tr>');
											});

											$settings.find('td table').append('<tr><td colspan="4"><i>THEN SHOW</i></td></tr>');

											ruleConf.Elements.forEach(function(elem){
												var elemName = elem.Name,
													variants = elem.Contents.map(function(variant){
														return variant.Name + ' (' + variant.Weight + ')';
													}).join(', ');

												$settings.find('td table').append('<tr><td width="25%">' + elemName + '</td><td colspan="3" width="75%">' + variants + '</td></tr>');
											});

											$content = $content.add($settings);
										}
									});
								}
							});

							summaryTable.append($content);
						}
					}
				}
			}
		],
		buildBrowserRulesItem = function(){
			var $content = $('<tr class="headline" style="background-color: #555; color: #fff;"><th colspan="2">Browser Rules</th></tr><tr class="subline" style="background-color: #dadada; color: #525252;"><th>Campaign Level</th><th>Site Level</th></tr>'),
				$settings = $('<tr class="settings"><td class="camp-lvl"><div class="mm-include"></div><div class="mm-exclude"></div></td><td class="site-lvl"><div class="mm-include"></div><div class="mm-exclude"></div></td></tr>');

			$('.mm-CampaignTargeting .browser-rule-items .dashbrd-lnk-std').each(function(){
				var itemType = $(this).closest('.mm-include').length ? 'include' : 'exclude',
					itemLevel = $(this).hasClass('free-left-side') ? 'site' : 'camp';

				$settings.find('.' + itemLevel + '-lvl .mm-' + itemType).append($(this));
				$content = $content.add($settings);
			});

			$content.find('.mm-exclude .dashbrd-lnk-std').css('color', 'red');	//fix for import to the ms excel
			summaryTable.append($content);
		},
		style = 'table.summary th{font-weight: bold} table.summary tr{border: 1px dotted #000} table.summary th, table.summary td{vertical-align: top; padding: 5px 10px; border-left: 1px dotted #000} table.summary th:first-child, table.summary td:first-child{border-left: none} table.summary i{font-style: italic}';

	//initialize preloader
	$('.t-overlay').show();
	$.fancybox.showActivity();

	//initialize a deferred for the each item
	items.forEach(function(item){
		item.defer.getData();
		itemsQueue.push($.when.apply($, item.defer.queue));
	});

	//handle the deferred chain
	$.when.apply($, itemsQueue).done(function(){
		var args = [].slice.apply(arguments);

		items.forEach(function(item, i){
			item.parser(args[i]);
		});

		buildBrowserRulesItem();

		$('head').append('<style>' + style + '</style>');
		$('body').replaceWith(summaryTable);
	});
})(window.jQuery);
